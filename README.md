# openBIS UGM 2019

Material for workshops at the openBIS User Group Meeting 2019.

In **workshop 2** participants will need the following VM:

https://polybox.ethz.ch/index.php/s/cX7DTCdm9DoJmuT

The VM can be opened with the VirtualBox program (https://www.virtualbox.org/)

VM username/password = openbis/openbis

openBIS username/password= admin/openbis